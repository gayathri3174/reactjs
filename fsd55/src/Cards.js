import React from 'react';
import './Cards.css';

function Cards(props) {
  const data = props.data;


  return (
    <div>
      {data.map((item, index) => (
        <div key={index} className="card cardsection">
          <img
            className="card-img-top"
            style={{ height: 100, width: 100, margin: 'auto' }}
            src="fruits.jpeg"
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">{item.Productname}</h5>
            <p className="card-text">{item.productdiscription}</p>
            <a href="#" className="btn btn-primary">
              {item.buttontittle}
            </a>
          </div>
        </div>
      ))}
    </div>
  );
}

export default Cards;
