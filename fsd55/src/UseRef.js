import React, { useEffect, useRef, useState } from 'react'

const UseRef = () => {
    const [name, setName] = useState("");
    const username = useRef();
    const useremail = useRef();
    const count = useRef(0);
   

    useEffect(()=>{
        (username.current.focus());
     

    })
    const handleFocus = ()=>{
       username.current.focus()
      
    
     

    }

    const handleIncrement = () =>{
        console.log(count.current = count.current+1);
        
    }
return (
         <div>

            <input type='text' placeholder='Name' onChange={(e)=>{setName(e.target.value)}} ref={username}/>
            <input type='text' placeholder='email'  ref={useremail}/>
             <button onClick={handleFocus}>Submit</button>

             
             <button onClick={handleIncrement}>increment</button>

      </div>
    )
}


export default UseRef