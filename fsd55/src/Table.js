import React, { Component } from 'react';

export default class Table extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            participantdetails: [
                {
                    id: 101,
                    name: 'harish',
                    batch: '55',
                    performance: 'good'
                },
                {
                    id: 102,
                    name: 'Ashok',
                    batch: '55',
                    performance: 'good'
                },
                {
                    id: 103,
                    name: 'Sai',
                    batch: '55',
                    performance: 'good'
                },
                {
                    id: 104,
                    name: 'Mahesh',
                    batch: '55',
                    performance: 'good'
                },
                {
                    id: 106,
                    name: 'Akhil',
                    batch: '55',
                    performance: 'good'
                },
                {
                    id: 107,
                    name: 'Akhil',
                    batch: '55',
                    performance: 'good'
                }
            ]
        };
    }

    render() {
      

        return (
            <div >
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Batch</th>
                            <th>Performance</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.participantdetails.map((participant, index) => (
                            <tr key={index}>
                                <td>{participant.id}</td>
                                <td>{participant.name}</td>
                                <td>{participant.batch}</td>
                                <td>{participant.performance}</td>
                            </tr>
                        ))}

                    </tbody>
                </table>
                
            </div>
        );
    }
}
