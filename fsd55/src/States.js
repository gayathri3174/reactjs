import React, { useState } from 'react'

const 
States = () => {
  const [count,setCount] = useState(0);
  const Increment = ()=>{
    setCount(count+1);
  }
  return (
    <div>
      <h1>count {count}</h1>
      <button onClick = {Increment}>Increment</button>
   
    </div>
  )
}

export default States