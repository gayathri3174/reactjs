import React, { Component } from 'react'

export class ClassStates extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         count :0,
      }
    }

  increment = ()=>{
    this.setState({count:this.state.count+1})
        
    }
   
  render() {

    return (
    <div>
    <div>ClassStates {this.state.count}</div>

      <button onClick={this.increment}>Click </button>
    </div>

    )
  }
}

export default ClassStates